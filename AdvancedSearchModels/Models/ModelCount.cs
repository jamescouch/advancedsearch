﻿namespace AdvancedSearchModels.Models
{
    public class ModelCount
    {
        public string ModelName { get; set; }

        /// <summary>
        /// This is the 8 in 8/9
        /// </summary>
        public int ActualAmount { get; set; }

        /// <summary>
        /// This is the 9 in 8/9
        /// </summary>
        public int TotalCount { get; set; }
    }
}

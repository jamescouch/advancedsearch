﻿using System.Collections.Generic;

namespace AdvancedSearchModels.Models
{
    public class Query
    {
        public string Category { get;set; }
        public string SearchType { get; set; }
        public string SearchValue { get; set; }
        public List<Query> ChildQueries { get; set; } = new List<Query>();
    }
}

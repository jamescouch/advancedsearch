﻿using System.ComponentModel.DataAnnotations;
using AdvancedSearchModels.Types;

namespace AdvancedSearchModels.Models
{
    public class KeywordSearch
    {
        public KeywordSearchTypes? SearchType { get; set; }

        [Display(Name = "Keyword Search")]
        public string SearchValue { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace AdvancedSearchModels.Models
{
    public class TableAndColumns
    {
        public string Table { get; set; }

        public List<string> Columns { get; set; } = new List<string>();
    }
}

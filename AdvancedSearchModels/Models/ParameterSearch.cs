﻿using AdvancedSearchModels.Types;
using System.ComponentModel.DataAnnotations;

namespace AdvancedSearchModels.Models
{
    public class ParameterSearch
    {
        public ParameterSearchTextTypes? SearchTextType { get; set; }

        public ParameterSearchNumberTypes? SearchNumberType { get; set; }

        [Display(Name = "Parameter Search")]
        public string SearchValue { get; set; }

        public string ChosenColumnName { get; set; }
    }
}

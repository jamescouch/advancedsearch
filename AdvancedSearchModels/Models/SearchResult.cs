﻿using System.Collections.Generic;

namespace AdvancedSearchModels.Models
{
    public class SearchResult
    {
        public List<string> Categories { get; set; } = new List<string>();
        public List<ModelCount> ModelCounts { get; set; } = new List<ModelCount>();
    }
}

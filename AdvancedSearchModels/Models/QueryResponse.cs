﻿namespace AdvancedSearchModels.Models
{
    public class QueryResponse
    {
        /// <summary>
        /// Return actual data instead of this string
        /// </summary>
        public string Response { get;set; }
    }
}

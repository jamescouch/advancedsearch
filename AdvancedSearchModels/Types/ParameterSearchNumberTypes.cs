﻿using System.ComponentModel.DataAnnotations;

namespace AdvancedSearchModels.Types
{
    public enum ParameterSearchNumberTypes
    {
        [Display(Name = "=")]
        Equals,
        [Display(Name = ">")]
        GreaterThan,
        [Display(Name = "<")]
        LessThan,
        [Display(Name = ">=")]
        GreaterThanOrEqual,
        [Display(Name = "<=")]
        LessThanOrEqual,
        Between
    }
}
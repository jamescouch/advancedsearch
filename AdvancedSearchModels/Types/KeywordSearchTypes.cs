﻿using System.ComponentModel.DataAnnotations;

namespace AdvancedSearchModels.Types
{
    public enum KeywordSearchTypes
    {
        Equals,
        Contains,
        [Display(Name="Begins With")]
        BeginsWith,
        [Display(Name="Ends With")]
        EndsWith
    }
}
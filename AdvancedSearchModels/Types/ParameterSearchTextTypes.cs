﻿using System.ComponentModel.DataAnnotations;

namespace AdvancedSearchModels.Types
{
    public enum ParameterSearchTextTypes
    {
        Equals,
        Contains,
        [Display(Name = "Begins With")]
        BeginsWith,
        [Display(Name = "Ends With")]
        EndsWith,
        [Display(Name = "Does Not Contain")]
        DoesNotContain,
        [Display(Name = "Is Null")]
        IsNull,
        [Display(Name = "Is Not Null")]
        IsNotNull
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using AdvancedSearchMvc.ViewModels;

namespace AdvancedSearchMvc.Controllers
{
    public class AdvancedSearchController : Controller
    {
        public IActionResult Index(AdvancedSearchViewModel viewModel)
        {
            return View(viewModel);
        }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AdvancedSearchModels.Models;

namespace AdvancedSearchMvc.ViewModels
{
    public class AdvancedSearchViewModel
    {
        public KeywordSearch KeywordSearch { get; set; }

        [Display(Name = "Parameter Search")]
        public string TablesAndColumnsParameterSearchValue { get; set; }

        public ParameterSearch ParameterSearch { get; set; }

        public List<Query> Queries { get; set; } = new List<Query>();
    }
}
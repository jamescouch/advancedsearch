﻿using System.Collections.Generic;
using AdvancedSearchApi.FakeData.Models;

namespace AdvancedSearchApi.Repositories.Interfaces
{
    public interface IDataRepository
    {
        List<SiteData> GetSiteData();
    }
}

﻿using System.Collections.Generic;
using AdvancedSearchModels.Models;

namespace AdvancedSearchApi.Repositories.Interfaces
{
    public interface IParameterSearchRepository
    {
        /// <summary>
        /// This should returns a list of columns (as well as the tables that
        /// contain those columns) that contain the entered text.
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        List<TableAndColumns> GetTablesAndColumns(string searchValue);

        /// <summary>
        /// This should return a list of potential matches according to the user's
        /// chosen search type and value and column (For text: Equals, Contains,
        /// Begins With, Ends With, Does Not Contain, Is Null, Is Not Null).
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        List<string> GetParametersTextSearch(ParameterSearch searchModel);

        /// <summary>
        /// This should return a list of potential matches according to the user's
        /// chosen search type and value and column (For numbers: Equals, Less
        /// Than, Greater Than, Less Than or Equal To, Greater Than or Equal To,
        /// Between).
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        List<string> GetParametersNumberSearch(ParameterSearch searchModel);
    }
}

﻿using System.Collections.Generic;
using AdvancedSearchModels.Models;

namespace AdvancedSearchApi.Repositories.Interfaces
{
    public interface IKeywordSearchRepository
    {
        /// <summary>
        /// This should return the names of the columns that contain data that
        /// match the user's chosen search type and value (according to the
        /// chosen search type of Equals, Contains, Begins With, Ends With).
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        List<string> GetCategories(KeywordSearch searchModel);
    }
}

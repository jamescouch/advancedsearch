﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace AdvancedSearchApi.Helpers
{
    public class DisplayNameHelper
    {
        public static string GetDisplayName(Type type, string attributeName)
        {
            var property = type.GetProperty(attributeName);
            var display = property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
            return display?.Name ?? property.Name;
        }
    }
}

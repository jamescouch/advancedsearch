﻿namespace AdvancedSearchApi.FakeData.Models
{
    public class SolrResponseHeader
    {
        public int Status { get; set; }
        public int QTime { get; set; }
        public SolrQueryParams Params { get; set; }
    }
}

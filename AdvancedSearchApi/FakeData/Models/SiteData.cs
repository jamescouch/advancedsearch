﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdvancedSearchApi.FakeData.Models
{
    public class SiteData
    {
        public int SiteId { get; set; }

        [Display(Name = "State Province")]
        public string StateProvince { get; set; }

        [Display(Name = "Basin")]
        public string BasinName { get; set; }

        public string Well { get; set; }

        public string Field { get; set; }

        public string Country { get; set; }

        [Display(Name = "Sub Basin")]
        public string SubBasinName { get; set; }

        [Display(Name = "USGS Province")]
        public string UsgsProvince { get; set; }

        [Display(Name = "Total Depth")]
        public double TotalDepth { get; set; }

        [Display(Name = "Site Alias")]
        public string SiteIdAlias { get; set; }

        [Display(Name = "County")]
        public string CountyBlock { get; set; }

        public List<string> Products { get; set; } = new List<string>();

        public List<string> Samples { get; set; } = new List<string>();
    }
}

﻿namespace AdvancedSearchApi.FakeData.Models
{
    public class SolrResponse
    {
        public SolrResponseHeader ResponseHeader { get; set; }
        public SolrResponseBody Response { get; set; }
    }
}

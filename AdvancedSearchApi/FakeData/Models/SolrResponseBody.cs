﻿using System.Collections.Generic;

namespace AdvancedSearchApi.FakeData.Models
{
    public class SolrResponseBody
    {
        public int NumFound { get; set; }
        public int Start { get; set; }
        public List<SiteData> Docs { get; set; } = new List<SiteData>();
    }
}
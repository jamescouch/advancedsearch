﻿namespace AdvancedSearchApi.FakeData.Models
{
    public class SolrQueryParams
    {
        public string Q { get; set; }
        public string Rows { get; set; }
    }
}

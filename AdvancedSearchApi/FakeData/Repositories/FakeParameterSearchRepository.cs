﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdvancedSearchApi.FakeData.Models;
using AdvancedSearchApi.Helpers;
using AdvancedSearchApi.Repositories.Interfaces;
using AdvancedSearchModels.Models;
using AdvancedSearchModels.Types;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace AdvancedSearchApi.FakeData.Repositories
{
    public class FakeParameterSearchRepository : IParameterSearchRepository
    {
        private IDataRepository DataRepository { get; }

        public FakeParameterSearchRepository(IDataRepository dataRepository)
        {
            DataRepository = dataRepository;
        }

        public List<TableAndColumns> GetTablesAndColumns(string searchValue)
        {
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var tablesAndColumns = new List<TableAndColumns>
            {
                new TableAndColumns
                {
                    Table = "Site Data",
                    Columns = new List<string>()
                }
            };

            var properties = typeof(SiteData).GetProperties();
            foreach (var property in properties)
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), property.Name);
                var displayNameMatches = displayName.ToLower().Contains(searchValue);
                var propertyNameMatches = property.Name.ToLower().Contains(searchValue);

                if (string.IsNullOrWhiteSpace(searchValue) || displayNameMatches || propertyNameMatches)
                {
                    tablesAndColumns[0].Columns.Add(displayName);
                }
            }

            return tablesAndColumns;
        }

        public List<string> GetParametersTextSearch(ParameterSearch searchModel)
        {
            var siteData = DataRepository.GetSiteData();
            var results = new List<string>();

            switch (searchModel.SearchTextType)
            {
                case ParameterSearchTextTypes.Equals:
                    results = GetParametersFromColumnThatHaveValuesThatAreEqualTo(searchModel.SearchValue,
                        searchModel.ChosenColumnName, siteData);
                    break;
                case ParameterSearchTextTypes.Contains:
                    results = GetParametersFromColumnThatHaveValuesThatContain(searchModel.SearchValue,
                        searchModel.ChosenColumnName, siteData);
                    break;
                case ParameterSearchTextTypes.BeginsWith:
                    results = GetParametersFromColumnThatHaveValuesThatBeginWith(searchModel.SearchValue,
                        searchModel.ChosenColumnName, siteData);
                    break;
                case ParameterSearchTextTypes.EndsWith:
                    results = GetParametersFromColumnThatHaveValuesThatEndWith(searchModel.SearchValue,
                        searchModel.ChosenColumnName, siteData);
                    break;
                case ParameterSearchTextTypes.DoesNotContain:
                    results = GetParametersFromColumnThatHaveValuesThatDoNotContain(searchModel.SearchValue,
                        searchModel.ChosenColumnName, siteData);
                    break;
                case ParameterSearchTextTypes.IsNull:
                    results = GetParametersFromColumnThatHaveValuesThatAreNull();
                    break;
                case ParameterSearchTextTypes.IsNotNull:
                    results = GetParametersFromColumnThatHaveValuesThatAreNotNull(searchModel.ChosenColumnName, siteData);
                    break;
            }

            results.Sort();

            return results;
        }

        public List<string> GetParametersNumberSearch(ParameterSearch searchModel)
        {
            return new List<string>();
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatAreEqualTo(string searchValue,
            string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var result = dataForChosenColumn
                .Where(x => x?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false)
                .Distinct().ToList();

            return result;
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatContain(string searchValue,
            string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var result = dataForChosenColumn
                .Where(x => x?.ToLower().Contains(searchValue) ?? false)
                .Distinct().ToList();

            return result;
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatBeginWith(string searchValue,
            string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var result = dataForChosenColumn
                .Where(x => x?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false)
                .Distinct().ToList();

            return result;
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatEndWith(string searchValue,
            string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var result = dataForChosenColumn
                .Where(x => x?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false)
                .Distinct().ToList();

            return result;
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatDoNotContain(string searchValue,
            string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            searchValue = searchValue?.ToLower() ?? string.Empty;

            var result = dataForChosenColumn
                .Where(x => (x?.ToLower().Contains(searchValue) ?? false) == false)
                .Distinct().ToList();

            return result;
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatAreNull()
        {
            return new List<string>();
        }

        private static List<string> GetParametersFromColumnThatHaveValuesThatAreNotNull(string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            var dataForChosenColumn = GetDataForColumn(columnName, siteData);
            return dataForChosenColumn.ToList();
        }

        private static bool ColumnNameMatches(string columnName, string propertyName)
        {
            var lowerCaseColumnName = columnName.ToLower();

            var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), propertyName);
            var isCorrectDisplayName = displayName.ToLower().Contains(lowerCaseColumnName);
            var isCorrectPropertyName = propertyName.ToLower().Contains(lowerCaseColumnName);

            return isCorrectDisplayName || isCorrectPropertyName;
        }

        private static IEnumerable<string> GetDataForColumn(string columnName, IReadOnlyCollection<SiteData> siteData)
        {
            IEnumerable<string> result = new List<string>();

            if (ColumnNameMatches(columnName, nameof(SiteData.SubBasinName)))
            {
                result = siteData.Select(x => x.SubBasinName);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.CountyBlock)))
            {
                result = siteData.Select(x => x.CountyBlock);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.Field)))
            {
                result = siteData.Select(x => x.Field);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.SiteIdAlias)))
            {
                result = siteData.Select(x => x.SiteIdAlias);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.StateProvince)))
            {
                result = siteData.Select(x => x.StateProvince);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.UsgsProvince)))
            {
                result = siteData.Select(x => x.UsgsProvince);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.Well)))
            {
                result = siteData.Select(x => x.Well);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.BasinName)))
            {
                result = siteData.Select(x => x.BasinName);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.Country)))
            {
                result = siteData.Select(x => x.Country);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.Products)))
            {
                result = siteData.SelectMany(x => x.Products);
            }
            else if (ColumnNameMatches(columnName, nameof(SiteData.Samples)))
            {
                result = siteData.SelectMany(x => x.Samples);
            }

            return result.Where(x => !string.IsNullOrWhiteSpace(x)).Distinct();
        }
    }
}

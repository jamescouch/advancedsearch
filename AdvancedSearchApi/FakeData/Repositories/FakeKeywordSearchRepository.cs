﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdvancedSearchApi.FakeData.Models;
using AdvancedSearchApi.Helpers;
using AdvancedSearchApi.Repositories.Interfaces;
using AdvancedSearchModels.Models;
using AdvancedSearchModels.Types;

namespace AdvancedSearchApi.FakeData.Repositories
{
    public class FakeKeywordSearchRepository : IKeywordSearchRepository
    {
        private IDataRepository DataRepository { get; }

        public FakeKeywordSearchRepository(IDataRepository dataRepository)
        {
            DataRepository = dataRepository;
        }

        public List<string> GetCategories(KeywordSearch searchModel)
        {
            var siteData = DataRepository.GetSiteData();
            var categories = new List<string>();

            switch (searchModel.SearchType)
            {
                case KeywordSearchTypes.Equals:
                    categories = GetKeywordCategoriesThatHaveValuesThatAreEqualTo(searchModel.SearchValue, siteData);
                    break;
                case KeywordSearchTypes.BeginsWith:
                    categories = GetKeywordCategoriesThatHaveValuesThatBeginWith(searchModel.SearchValue, siteData);
                    break;
                case KeywordSearchTypes.EndsWith:
                    categories = GetKeywordCategoriesThatHaveValuesThatEndWith(searchModel.SearchValue, siteData);
                    break;
                case KeywordSearchTypes.Contains:
                    categories = GetKeywordCategoriesThatHaveValuesThatContain(searchModel.SearchValue, siteData);
                    break;
            }

            return categories;
        }

        private static List<string> GetKeywordCategoriesThatHaveValuesThatAreEqualTo(string searchValue, IReadOnlyCollection<SiteData> siteData)
        {
            var categories = new List<string>();

            if (siteData.Any(x => x.BasinName?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.BasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Country?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Country));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SubBasinName?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SubBasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.CountyBlock?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.CountyBlock));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Field?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Field));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SiteIdAlias?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SiteIdAlias));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.StateProvince?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.StateProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.UsgsProvince?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.UsgsProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Well?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Well));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Products?.Any(y => y?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Products));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Samples?.Any(y => y?.Equals(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Samples));
                categories.Add(displayName);
            }

            return categories;
        }

        private static List<string> GetKeywordCategoriesThatHaveValuesThatBeginWith(string searchValue, IReadOnlyCollection<SiteData> siteData)
        {
            var categories = new List<string>();

            if (siteData.Any(x => x.BasinName?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.BasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Country?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Country));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SubBasinName?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SubBasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.CountyBlock?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.CountyBlock));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Field?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Field));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SiteIdAlias?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SiteIdAlias));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.StateProvince?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.StateProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.UsgsProvince?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.UsgsProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Well?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Well));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Products?.Any(y => y?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Products));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Samples?.Any(y => y?.StartsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Samples));
                categories.Add(displayName);
            }

            return categories;
        }

        private static List<string> GetKeywordCategoriesThatHaveValuesThatEndWith(string searchValue, IReadOnlyCollection<SiteData> siteData)
        {
            var categories = new List<string>();

            if (siteData.Any(x => x.BasinName?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.BasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Country?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Country));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SubBasinName?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SubBasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.CountyBlock?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.CountyBlock));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Field?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Field));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SiteIdAlias?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SiteIdAlias));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.StateProvince?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.StateProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.UsgsProvince?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.UsgsProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Well?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Well));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Products?.Any(y => y?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Products));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Samples?.Any(y => y?.EndsWith(searchValue, StringComparison.InvariantCultureIgnoreCase) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Samples));
                categories.Add(displayName);
            }

            return categories;
        }

        private static List<string> GetKeywordCategoriesThatHaveValuesThatContain(string searchValue, IReadOnlyCollection<SiteData> siteData)
        {
            var categories = new List<string>();
            searchValue = searchValue?.ToLower();

            if (siteData.Any(x => x.BasinName?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.BasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Country?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Country));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SubBasinName?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SubBasinName));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.CountyBlock?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.CountyBlock));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Field?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Field));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.SiteIdAlias?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.SiteIdAlias));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.StateProvince?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.StateProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.UsgsProvince?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.UsgsProvince));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Well?.ToLower()?.Contains(searchValue) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Well));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Products?.Any(y => y?.ToLower().Contains(searchValue) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Products));
                categories.Add(displayName);
            }

            if (siteData.Any(x => x.Samples?.Any(y => y?.ToLower().Contains(searchValue) ?? false) ?? false))
            {
                var displayName = DisplayNameHelper.GetDisplayName(typeof(SiteData), nameof(SiteData.Samples));
                categories.Add(displayName);
            }

            return categories;
        }
    }
}

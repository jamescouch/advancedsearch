﻿using System.Collections.Generic;
using System.IO;
using AdvancedSearchApi.FakeData.Models;
using AdvancedSearchApi.Repositories.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace AdvancedSearchApi.FakeData.Repositories
{
    public class FakeDataRepository : IDataRepository
    {
        private IHostingEnvironment HostingEnvironment { get; }

        public FakeDataRepository(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }

        public List<SiteData> GetSiteData()
        {
            var sampleDataFile = File.ReadAllText(HostingEnvironment.ContentRootPath + @"/FakeData/Data/rfdbase_sites.json");
            var sampleData = JsonConvert.DeserializeObject<SolrResponse>(sampleDataFile);
            return sampleData.Response.Docs;
        }
    }
}

﻿using System.Collections.Generic;
using AdvancedSearchApi.Repositories.Interfaces;
using AdvancedSearchModels.Models;
using Microsoft.AspNetCore.Mvc;

namespace AdvancedSearchApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AdvancedSearchController : Controller
    {
        private IKeywordSearchRepository KeywordSearchRepository { get; }

        private IParameterSearchRepository ParameterSearchRepository { get; }

        public AdvancedSearchController(IDataRepository dataRepository,
            IKeywordSearchRepository keywordSearchRepository, IParameterSearchRepository parameterSearchRepository)
        {
            KeywordSearchRepository = keywordSearchRepository;
            ParameterSearchRepository = parameterSearchRepository;
        }

        /// <summary>
        /// As the user types into the keyword search input fields (there is one in
        /// the left panel and one in the center panel) this should return the names
        /// of the columns that match the user's chosen search type (according to the
        /// chosen search type of Equals, Contains, Begins With, Ends With).
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpGet]
        public SearchResult KeywordSearch(KeywordSearch searchModel)
        {
            var categories = KeywordSearchRepository.GetCategories(searchModel);

            return new SearchResult
            {
                Categories = categories
            };
        }

        /// <summary>
        /// As the user types into the parameter search input field (we are talking
        /// about the one and it is in the left panel below the keyword search input
        /// field) this should returns a list of columns (as well as the tables that
        /// contain those columns) that contain the entered text.
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        [HttpGet]
        public List<TableAndColumns> TablesAndColumnsSearch(string searchValue)
        {
            var tablesAndColumns = ParameterSearchRepository.GetTablesAndColumns(searchValue);

            return tablesAndColumns;
        }

        /// <summary>
        /// As the user types into the parameter search input field (we are talking
        /// about the one located in the center panel) this should return a list of
        /// potential matches according to the user's chosen search type
        /// (For text: Equals, Contains, Begins With, Ends With, Does Not Contain,
        /// Is Null, Is Not Null)
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpGet]
        public SearchResult ParameterTextSearch(ParameterSearch searchModel)
        {
            var categories = ParameterSearchRepository.GetParametersTextSearch(searchModel);

            return new SearchResult
            {
                Categories = categories
            };
        }

        /// <summary>
        /// As the user types into the parameter search input field (we are talking
        /// about the one located in the center panel) this should return a list of
        /// potential matches according to the user's chosen search type
        /// (For numbers: Equals, Less Than, Greater Than, Less Than or Equal To,
        /// Greater Than or Equal To, Between)
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [HttpGet]
        public SearchResult ParameterNumberSearch(ParameterSearch searchModel)
        {
            var categories = new List<string>();
            //var categories = ParameterSearchRepository.GetCategories(searchModel.SearchNumberType, searchModel.SearchValue);

            return new SearchResult
            {
                Categories = categories
            };
        }

        [HttpPost]
        public QueryResponse Query([FromBody] List<Query> queries)
        {
            var totalMainQueries = queries.Count;
            var totalChildrenQueries = 0;

            foreach (var query in queries)
            {
                totalChildrenQueries += query.ChildQueries.Count;
            }

            var totalQueries = totalMainQueries + totalChildrenQueries;

            return new QueryResponse
            {
                Response =
                    $"Query received: Total Queries: {totalQueries}\nTotal Main Queries: {totalMainQueries}\nTotal Children Queries: {totalChildrenQueries}"
            };
        }
    }
}
